//
//  Emoji.swift
//  EmojiApp
//
//  Created by Daniel Esposito on 10/21/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

class Emoji {
    
    var stringEmoji = ""
    var emojiDefinition = ""
    var emojiCategory = ""
    var emojiBirthYear = 0
}
