
//
//  EmojiCell.swift
//  EmojiApp
//
//  Created by Daniel Esposito on 10/20/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit

class EmojiCell: UITableViewCell {
    @IBOutlet weak var emojiCellIcon: UILabel!
    @IBOutlet weak var emojiCellName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
