//
//  ViewController.swift
//  EmojiApp
//
//  Created by Daniel Esposito on 10/20/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved. Lalala
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var emojiTable: UITableView!
    
    var emojiCharacter = ["😀","😁","😂","😃","😄","😄","😅","😆","😇","😈","😊","🙂"]
    var emojiNames = ["Grinning Face","Grinning Face with Teeth","Face & Tears","Friendly Smile","Big Eyes Smile","Sweat Laughing","Squinted Eyes","Hollow Smile","Purple Face","Red Face","Sunshine Face", "Confused Face"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emojiTable.delegate = self
        emojiTable.dataSource = self
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emojiCharacter.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let emoName = emojiCharacter[indexPath.row]
        let emoInfo = emojiNames[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: "emojiCell") as? EmojiCell {
            cell.emojiCellIcon.text = "\(emoName)"
            cell.emojiCellName.text = "\(emoInfo)"
            return cell
            
        } else {
            
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let daEmojiSender = emojiCharacter[indexPath.row]
        let daEmojiSender2 = emojiNames[indexPath.row]
        performSegue(withIdentifier: "EmojiVC", sender: [daEmojiSender,daEmojiSender2])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "EmojiVC" {
            let send = segue.destination as! EmojiVC
                send.emojiArray = (sender as! NSArray) as! [String]
        }
    }
    
    func makeEmojiArray() -> [Emoji]{
        
        return []
        
    }
}

