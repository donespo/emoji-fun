//
//  EmojiVC.swift
//  EmojiApp
//
//  Created by Daniel Esposito on 10/20/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit

class EmojiVC: UIViewController {

    @IBOutlet weak var defEmojiIcon: UILabel!
    @IBOutlet weak var defEmojiName: UILabel!
    @IBOutlet weak var defCategoryLabel: UILabel!
    @IBOutlet weak var defBirthYearLabel: UILabel!

    var emojiArray = [""]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        defEmojiIcon.text = emojiArray[0]
        defEmojiName.text = emojiArray[1]
        // Do any additional setup after loading the view.
    }
}
